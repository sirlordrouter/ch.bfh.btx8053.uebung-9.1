import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class CreateLocalDb {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		
		String DB_FILE = "./storage/uebungsDB"; 
		Connection con = DriverManager.getConnection(
				"jdbc:hsqldb:file:" + DB_FILE + ";ifexists=true;shutdown=true", "SA", "");
		
		Statement state = con.createStatement();
		
		String create = "Create Table PUBLIC.BodyTemperature (" +
		"PatientNb integer NOT NULL, " +
		"CaseNb integer NOT NULL, " +
		"DateAndTime datetime NOT NULL, " +
		"Temperature decimal(4,2) NOT NULL, " +
		")";
		
		
		ResultSet result = state.executeQuery(create);
		
		state.close();
		con.close();
		
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
