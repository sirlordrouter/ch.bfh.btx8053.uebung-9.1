import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class PerformanceTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Connection connSQL;
		try {
			
			/**
			 * SQL Query to Test
			 */
			String btsql = "select * from BodyTemperature";
			
			
			/**************************************** SQL Server ******************************************************/
			
			connSQL = DriverManager.getConnection(
					"jdbc:jtds:sqlserver://corpus.bfh.ch:55783;DatabaseName=Medinf",
					"scott", "tiger");
			
			Statement stSQL = connSQL.createStatement();
			
			Long startTimeSQL = System.currentTimeMillis();
			
			ResultSet resultSQL = stSQL.executeQuery(btsql);
			
			while (resultSQL.next()) {
				String a = resultSQL.getInt(1) + ", " + 
						resultSQL.getInt(2) + ", " + 
						resultSQL.getDate(3) + ", " + 
						resultSQL.getFloat(4);
			}
			
			System.out.println("Execution time for SQL: " + (System.currentTimeMillis() - startTimeSQL));
			
			stSQL.close();
			connSQL.close();
			
			/**************************************** Local DB ******************************************************/
			
			String DB_FILE = "./storage/uebungsDB"; 
			Connection connLocal = DriverManager.getConnection(
					"jdbc:hsqldb:file:" + DB_FILE + ";ifexists=true;shutdown=true", "SA", "");
			
			Statement stLocal = connLocal.createStatement();
			
			Long startTimeLocal = System.currentTimeMillis();
			
			ResultSet resultLocal = stLocal.executeQuery(btsql);
			
			while (resultLocal.next()) {
				String a = resultLocal.getInt(1) + ", " + 
						resultLocal.getInt(2) + ", " + 
						resultLocal.getDate(3) + ", " + 
						resultLocal.getFloat(4);
			}
			
			System.out.println("Execution time for Local: " + (System.currentTimeMillis() - startTimeLocal));
			
			stLocal.close();
			connLocal.close();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
