import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class CopyData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			
			/**
			 * Remote DB
			 */
			Connection conn = DriverManager.getConnection(
					"jdbc:jtds:sqlserver://corpus.bfh.ch:55783;DatabaseName=Medinf",
					"scott", "tiger");
			
			Statement st = conn.createStatement();
			
			String btsql = "select * from BodyTemperature";
			ResultSet resultBT = st.executeQuery(btsql);
			
			
			/**
			 * Local DB
			 */
			String DB_FILE = "./storage/uebungsDB"; 
			Connection con = DriverManager.getConnection(
					"jdbc:hsqldb:file:" + DB_FILE + ";ifexists=true;shutdown=true", "SA", "");
			
			String psql = "insert into BodyTemperature (PatientNb, CaseNb, DATEANDTIME, TEMPERATURE) values (?,?,?,?)";
			
			PreparedStatement pst = con.prepareStatement(psql);
			
			
			/**
			 * Real Copy Data Part
			 */
			while (resultBT.next()) {
				System.out.println(resultBT.getInt(1) + ", " + 
						resultBT.getInt(2) + ", " + 
						resultBT.getDate(3) + ", " + 
						resultBT.getFloat(4));
				
				pst.setInt(1, resultBT.getInt(1));
				pst.setInt(2, resultBT.getInt(2));
				pst.setDate(3, resultBT.getDate(3));
				pst.setDouble(4, resultBT.getFloat(4));
				
				System.out.println(pst.executeUpdate());
				
			}
			
			/**
			 * Close everything to be happy
			 */
			st.close();
			conn.close();	
			
			pst.close();
			con.commit();
			con.close();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
