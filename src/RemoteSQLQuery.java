import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;


public class RemoteSQLQuery {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			
			/**
			 * Open the connection to the Remote Server
			 */
			Connection conn = DriverManager.getConnection(
					"jdbc:jtds:sqlserver://corpus.bfh.ch:55783;DatabaseName=Medinf",
					"scott", "tiger");
			
			/******************** Query Patient Information normal Statement ****************/
					
			/**
			 * Setup Select Statement and Object Reference
			 */
			String sql = "Select PatientNb, Name, Firstname from Patient";
			Statement st = conn.createStatement();
			
			/**
			 * execute the Statement
			 */
			ResultSet result = st.executeQuery(sql);
			
			while (result.next()) {
				String id = result.getString(1);
				String name = result.getString(2);
				String firstName = result.getString(3);
				
				System.out.println(id + ", " + name + ", " + firstName);
			}
			
			
			/******************** Query Patient Information normal prepared Statement ********/
			String psql = "select PatientNb, Name, Firstname from Patient where Name = ?";
			
			PreparedStatement pst = conn.prepareStatement(psql);
			
			pst.setString(1, "Wenk");
			
			result = pst.executeQuery();
			
			while (result.next()) {
				String id = result.getString(1);
				String name = result.getString(2);
				String firstName = result.getString(3);
				
				System.out.println(id + ", " + name + ", " + firstName);
			}
			
			
			/******************** Query BodyTemperature Information  **************************/
			String btsql = "select * from BodyTemperature";
			ResultSet resultBT = st.executeQuery(btsql);
			
			while (resultBT.next()) {
				System.out.println(resultBT.getInt(1) + ", " + 
						resultBT.getInt(2) + ", " + 
						resultBT.getDate(3) + ", " + 
						resultBT.getFloat(4));			
			}
			
			/******************** Query BodyTemperature Metadata ********/
					
			ResultSetMetaData rsmd = resultBT.getMetaData();
			int colCount = rsmd.getColumnCount();
					
			for (int i = 1; i <= colCount; i++) {
				System.out.println(
				rsmd.getColumnName(i) +", "  +
				rsmd.getColumnTypeName(i) +", "  +
				rsmd.getSchemaName(i) +", "  +
				rsmd.getPrecision(i) +", "  +
				rsmd.isAutoIncrement(i)+", "  +
				rsmd.isNullable(i));	
			}
			
			/**
			 * Close the stuff to be happy :-)
			 */
			conn.close();
			st.close();
			pst.close();
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
